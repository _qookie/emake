#include <iostream>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <experimental/filesystem>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

namespace fs = std::experimental::filesystem;

struct target {

	std::string cxx_compiler;
	bool cxx_static_libraries = false;
	std::string cxx_libraries_directory = "";		// only valid if cxx_static_libraries==true
	std::string cxx_headers_directory = "";			// only valid if cxx_static_libraries==true
	std::string cxx_custom_compile_flags = "";
	std::string cxx_custom_link_flags = "";
	bool cxx_compile = true;
	bool cxx_link = true;
	
	bool copy_files = false;
	std::string files_to_copy;				// only valid if copy_files==true

};

struct project {

	std::string name = "";
	std::string cxx_std = "c++11";
	int cxx_optimization = 2;
	bool cxx_debug = false;
	std::string source_directory = "";
	std::string cxx_other_flags = "";
	std::string cxx_libraries = "";
	bool make_zipable = false;
	bool run_zip = false;

	std::unordered_map<std::string, target> targets;
	std::vector<std::string> target_names;

};

project _project;


std::vector<std::string> split(std::string str, std::string token){
    std::vector<std::string>result;
    while(str.size()){
        int index = str.find(token);
        if(index!=std::string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}

std::string trim(const std::string& str, char c) {
    size_t first = str.find_first_not_of(c);
    if (std::string::npos == first) {
        return str;
    }
    size_t last = str.find_last_not_of(c);
    return str.substr(first, (last - first + 1));
}

bool has_suffix(const std::string &str, const std::string &suffix) {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}

inline std::string param_str(std::string param) {
	return param.substr(1, param.size() - 2);
} 

inline bool param_bool(std::string param) {
	return param == "true";
} 

inline bool param_num(std::string param) {
	return std::stoi(param);
} 

bool dir_exists(std::string name) {
    struct stat info;

    if (stat(name.c_str(), &info) != 0)
        return false;
    else if (info.st_mode & S_IFDIR)
        return true;
    else
	    return false;
}

bool mkpath(std::string path) {
    bool bSuccess = false;
    #if defined(_WIN32)
    int nRC = ::mkdir( path.c_str());
    #else
    int nRC = ::mkdir( path.c_str(), 0775 );
    #endif
    if( nRC == -1 ) {
        switch( errno ) {
            case ENOENT:
                //parent didn't exist, try to create it
                if( mkpath( path.substr(0, path.find_last_of('/')) ) )
                    //Now, try to create again.
                    #if defined(_WIN32)
                    bSuccess = 0 == ::mkdir( path.c_str());
                    #else
                    bSuccess = 0 == ::mkdir( path.c_str(), 0775 );
                    #endif
                else
                    bSuccess = false;
                break;
            case EEXIST:
                //Done!
                bSuccess = true;
                break;
            default:
                bSuccess = false;
                break;
        }
    }
    else
        bSuccess = true;
    return bSuccess;
}

std::string objects;

bool compile_target(std::string target_name, std::string path_prefix) {
	target compile_target = _project.targets[target_name];

	mkpath(path_prefix + "/build/" + target_name + "/objects");
	mkpath(path_prefix + "/build/" + target_name + "/output");

	uint64_t nfile = 1;

	if (compile_target.cxx_compile) {

		for (auto & p : fs::recursive_directory_iterator(path_prefix + "/" + _project.source_directory)) {
	        
	        std::string pp = p.path();
	        if (pp.at(0) == '"')
	        	pp = pp.substr(_project.source_directory.size() + 1, pp.size() - 1);

	        std::string filename;
	        size_t pos = std::string(pp).find_last_of("/");
			if (pos < 0 || pos > pp.size()) filename = pp;
			else filename = pp.substr(pos, pp.size());


	        if (has_suffix(pp, ".cc") || has_suffix(pp, ".cpp") || has_suffix(pp, ".cxx")) {

		        std::cout << nfile++ << "\tCXX " << pp << "\n";
		
		        std::stringstream ss;
		        ss << compile_target.cxx_compiler << " -c ";
				ss << p.path() << " ";
				ss << "-std=" << _project.cxx_std << " ";
				if(_project.cxx_debug)
				ss << "-g ";
				ss << "-O" << _project.cxx_optimization << " ";
				ss << "-o \"" << path_prefix << "/build/" << target_name << "/objects/" << filename << ".o\" ";
				ss << _project.cxx_other_flags << "";
				
				if (compile_target.cxx_headers_directory.size() != 0) {
					ss << "-I" << compile_target.cxx_headers_directory << " ";
				}

				ss << compile_target.cxx_custom_compile_flags << " ";

		        // std::cout << ss.str() << "\n";

		        if(system(ss.str().c_str()) != 0) {
		        	return 1;
		        }

		        objects += path_prefix + "/build/" + target_name + "/objects/" + filename + ".o ";
		    }

		}
	} else {
		std::cout << "Compilation omited.\n";
	}

	return 0;
}

bool link_target(std::string target_name, std::string path_prefix) { 
	target compile_target = _project.targets[target_name];

	if (compile_target.cxx_link) {

	    std::stringstream ss;
	    ss << compile_target.cxx_compiler << " ";
		ss << objects;
		
		if (compile_target.cxx_static_libraries)
		ss << " -static -static-libgcc -static-libstdc++ ";

		ss << "-std=" << _project.cxx_std << " ";

		if(_project.cxx_debug)
		ss << "-g ";
		ss << "-O" << _project.cxx_optimization << " ";
		ss << "-o \"" << path_prefix << "/build/" << target_name << "/output/" << _project.name << "\" ";
		ss << _project.cxx_other_flags << "";
		
		std::vector<std::string> libs = split(_project.cxx_libraries, " ");
		for (std::string lib : libs)
			ss << "-l" << lib << " ";

		if (compile_target.cxx_libraries_directory.size() != 0) {
			ss << "-L" << compile_target.cxx_libraries_directory << " ";
		}

		if (compile_target.cxx_headers_directory.size() != 0) {
			ss << "-I" << compile_target.cxx_headers_directory << " ";
		}

		ss << compile_target.cxx_custom_link_flags << " ";

	    //std::cout << ss.str() << "\n";

	    if(system(ss.str().c_str()) != 0) {
			return 1;
		}
	} else {
		std::cout << "Linking omited.\n";
	}

	return 0;

}

bool copy_target(std::string target_name, std::string path_prefix) {
	target compile_target = _project.targets[target_name];

	if (compile_target.copy_files) {
		std::cout << "Starting prepackaging of target " << target_name << ".\n";

		std::vector<std::string> files = split(compile_target.files_to_copy, " ");
		
		for (std::string file : files)
			fs::copy(path_prefix + "/" + file, path_prefix + "/build/" + target_name + "/output/" + file, fs::copy_options::recursive | fs::copy_options::update_existing);

		std::cout << "Done prepackaging.\n";
	}

	return 0;
}

bool build_target(std::string target_name, std::string path_prefix) {

	std::cout << "Starting compilation of target " << target_name << ".\n";
	if (compile_target(target_name, path_prefix)) return 1;
	std::cout << "Done compiling.\n";

	std::cout << "Starting linking of target " << target_name << ".\n";
	if (link_target(target_name, path_prefix)) return 1;
	std::cout << "Done linking.\n";

	if (copy_target(target_name, path_prefix)) return 1;
	
	std::cout << "Done building target " << target_name << ".\n";

	return 0;

}

int main(int argc, char *argv[]) {
	
	if (argc < 2) {
		std::cerr << "emake: missing argument: filename\n";
		return EXIT_FAILURE;
	}

	if (argc < 3) {
		std::cerr << "emake: missing argument: target\n";
		return EXIT_FAILURE;
	}

	std::cout << "Hello!\n";
	std::cout << "emake 0.1\n";


	std::ifstream file(argv[1]);
	std::string str; 

	std::string _curr_target_name;
	target _target;
	bool reading_target = false;


	while(std::getline(file, str)) {

		// prepare line(split name and get values etc)

		if (str.size() > 0) {
			size_t pos = str.find_first_of(":");
			std::string name = trim(str.substr(0, pos), ' ');
			std::string param = trim(str.substr(pos + 1, str.size() - 1), ' ');
			
			name = trim(name, '\t');
			param = trim(param, '\t');

			//std::cout << "name: " << name << "\n";
			//std::cout << "param: " << param << "\n";

			if (name == "target") {
				// parsing target(create a target object, write to it, etc)
				reading_target = true;
			} else if (name == "name" && reading_target) 
				_curr_target_name = param_str(param);
			else if (name == "cxx-compiler" && reading_target) 
				_target.cxx_compiler = param_str(param);
			else if (name == "cxx-static-libraries" && reading_target) 
				_target.cxx_static_libraries = param_bool(param);
			else if (name == "cxx-libraries-directory" && reading_target) 
				_target.cxx_libraries_directory = param_str(param);
			else if (name == "cxx-headers-directory" && reading_target) 
				_target.cxx_headers_directory = param_str(param);
			else if (name == "copy-files" && reading_target) 
				_target.copy_files = param_bool(param);
			else if (name == "files-to-copy" && reading_target) 
				_target.files_to_copy = param_str(param);
			else if (name == "cxx-custom-compile-flags" && reading_target) 
				_target.cxx_custom_compile_flags = param_str(param);
			else if (name == "cxx-custom-link-flags" && reading_target) 
				_target.cxx_custom_link_flags = param_str(param);
			else if (name == "cxx-compile" && reading_target) 
				_target.cxx_compile = param_bool(param);
			else if (name == "cxx-link" && reading_target) 
				_target.cxx_link = param_bool(param);
			else if (name == "end" && reading_target) {
				_project.targets.emplace(_curr_target_name, _target);
				_project.target_names.push_back(_curr_target_name);
				reading_target = false;
			} else if (name == "name" && !reading_target) 
				_project.name = param_str(param);
			else if (name == "cxx-std" && !reading_target) 
				_project.cxx_std = param_str(param);
			else if (name == "cxx-debug" && !reading_target) 
				_project.cxx_debug = param_bool(param);
			else if (name == "cxx-optimization" && !reading_target) 
				_project.cxx_optimization = param_num(param);
			else if (name == "cxx-libraries" && !reading_target) 
				_project.cxx_libraries = param_str(param);
			else if (name == "cxx-other-flags" && !reading_target) 
				_project.cxx_other_flags = param_str(param);
			else if (name == "source-directory" && !reading_target) 
				_project.source_directory = param_str(param);
			else if (name == "make-zipable" && !reading_target) 
				_project.make_zipable = param_bool(param);
			else if (name == "run-zip" && !reading_target) 
				_project.run_zip = param_bool(param);
			else {
				std::cerr << "Unknown option: " << name << "\n";
			}


		}
	}

	std::cout << "Done reading project info.\n";

	std::string target_name = argv[2];

	std::string path_prefix;
	size_t pos = std::string(argv[1]).find_last_of("/");
	if (pos < 0 || pos > strlen(argv[1])) path_prefix = ".";
	else path_prefix = std::string(argv[1]).substr(0, pos);

	if (target_name == "all") {
		for (std::string t : _project.target_names) {
			build_target(t, path_prefix);
		}
	} else if (target_name == "zip-all") {
		for (std::string t : _project.target_names) {
			build_target(t, path_prefix);

			mkpath(path_prefix + "/zip/" + t);
			fs::copy(path_prefix + "/build/" + t + "/output", path_prefix + "/zip/" + t + "/", fs::copy_options::recursive | fs::copy_options::update_existing);
		}

	} else {
		build_target(target_name, path_prefix);
	}

	


}

/*




*/