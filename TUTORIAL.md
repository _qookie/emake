# Tutorial #

Looking at the sample.emake file, you can see a few things. Two targets and some variables being set.
This file is going to explain all the variables and basic usage of emake.

## Variables ##

Variables are defined like this:
```
name: value
```

Below are tables containing informations about all variables available.

These variables are global(ie not in the target)

Name             | Type    | Default value | Description          
---------------- | ------- | ------------- | ---------------------
name             | string  | ""            | Project name
cxx_std          | string  | "c++11"       | Compiler -std= flag
cxx_optimization | number  | 2             | Compiler -O flag
cxx_debug        | boolean | false         | Compiler -g flag
source_directory | string  | ""            | Directory contianing source code
cxx_other_flags  | string  | ""            | Additional compiler flags
cxx_libraries    | string  | ""            | Libraries to link, separated by spaces

These variables are used in targets

Name                     | Type    | Default value | Description          
------------------------ | ------- | ------------- | ---------------------
name                     | string  |               | Target name
cxx_compiler             | string  |               | Compiler to use when compiling this target
cxx_static_libraries     | boolean | false         | When true, adds `-static -static-libgcc -static-libstdc++` to linker flags
cxx_libraries_directory  | string  | ""            | When set, adds `-L<value>` to linker flags
cxx_headers_directory    | string  | ""            | When set, adds `-I<value>` to linker flags
cxx_custom_compile_flags | string  | ""            | Additional target specific compiler flags
cxx_custom_link_flags    | string  | ""            | Additional target specific linker flags
cxx_compile              | boolean | true          | When true, compiles source code
cxx_link                 | boolean | true          | When true, links object files
copy_files               | boolean | false         | When true, copies additional files/directories to output directory
files_to_copy            | string  | ""            | Space separated list of files and directories to copy

## Targets ##

Targets are well... compilation targets. Each target specifies a separate compiler, additional flags, etc.

The simplest target looks like this:
```
target:
	name: <name>
end
```

Pretty simple, isn't it. The name is always required(except if you don't want to use this target ;))
Every other variable defaults to the default value(except if it's not specified in the table).
You do not need to put the `:` after `target` or add tabs before variables.

There are also two built in targets(which are called even if you have a target with the same name), those are 
`all` and `zip-all`. The names are self explanatory, `all` compiles all targets and `zip-all` compiles all targets and creates a zip archive with them.

## Using emake ##

To use emake just run `emake <your config file> <target>`.

This will create a directory called `build` with some subdirectories.
The command outputs information about what it's currently doing, so when an error occurs, it will be simpler to find.

## Directory structure ##

After running emake, a directory called `build` will appear. This section explains the structure of this directory.

In `build`, there will be at least one directory, it's the target directory. Each target creates has it's own directory.

In the target directory, there will be two directories, `objects` and `output`. `objects` contains object files created when compiling source code. `output` contains the compiled executable and additional files copied when the target has `copy_files` enabled.

----------------------

`zip-all` also creates a different directory, `zip`, in the root if the folder(next to `build`) which contains directories for all targets and contents of their output folders. Next to that is a zip file with the name of your project, it contains all the contents of `zip`
