# emake #

emake is a simple build system for C++ projects. Currently emake is in version 0.2

### How do I get set up? ###

To set up the tool you need to first compile it.
There are no external dependencies, only a C++ compiler which supports C++17 and has `std::filesystem`

To compile it run this command(replace `g++` with your compiler)
`g++ emake.cc -o emake -std=c++17 -lstdc++fs`

Afterwards add the executable to your path or copy it to `/usr/bin`(not required, makes usage easier).

Now, to use emake you need a configuration file. You can take a look at sample.emake for a sample script.
For a small tutorial check out [TUTORIAL.md](TUTORIAL.md)
